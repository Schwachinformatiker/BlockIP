#!/usr/bin/env python
import socket

from sys import argv
from subprocess import call, DEVNULL
from netaddr import IPNetwork, IPAddress, AddrFormatError
# for ip in IPNetwork('0.0.0.0/23'): ...

# Requirements:
# Python:   socket, netaddr, subprocess, sys
# System:   sudo, systemctl, firewalld or ufw, 

# block the given IP after examine if the IP is valid and ufw or firewalld is active
# ip:     Array with IPv4 or IPv6 to block
def blockTheIP(ips: list) -> None:
    if not ips:
        print('The ips are not set')
        return
    
    firewall = checkFirewall()
    if not firewall == 'firewalld' and not firewall == 'ufw':
        return
    
    iptype = ''
    firewallWasUsed = 0
    print(ips)
    for ip in ips:
        if checkIP(ip) == 4:
            iptype = 'ipv4'
        elif checkIP(ip) == 6:
            iptype = 'ipv6'
        else:
            print('The given IP is not valid!', ip)
            continue
        if firewall == 'firewalld':
            if useFirewalld(iptype, ip):
                firewallWasUsed += 1
        elif firewall == 'ufw':
            if useUfw(ip):
                firewallWasUsed += 1
        else:
            print('Your firewall seems to be not active or is not installed! Please make shure you use ufw or firewalld!')
            return
    
    # reload firewall after write the rules
    if firewall == 'firewalld' and firewallWasUsed > 0:
        call('sudo firewall-cmd --reload', shell=True)
    elif firewall == 'ufw' and firewallWasUsed > 0:
        call('sudo ufw reload', shell=True)
    elif (firewall == 'firewalld' or firewall == 'ufw') and firewallWasUsed == 0:
        print('None of the given IP addresses is valid!')
        return
    else:
        print('Your firewall seems to be not active or is not installed! Please make shure you use ufw or firewalld!')
        return


# check if the given argument is a valid IPv4 or IPv6
# ip:       IP address to check for if its a valid IPv4 or IPv6
# @return:  Retuns False if the ip is not valid or the type of the ip as a integer
def checkIP(ip: str) -> int or bool:
    if ip:
        try:
            return IPAddress(ip).version
        except AddrFormatError as e:
            return False
    else:
        return False


# use the firewalld to block the IP
# iptype:   Type of the blocked IP
# ip:       IP to block
def useFirewalld(iptype: str, ip: str) -> bool:
    if not iptype or not ip:
        return False
    # check if every comand is realy called successfully
    noerror = call('sudo firewall-cmd --direct --permanent --add-rule {} filter OUTPUT 0 -d {} -j REJECT'.format(iptype, ip) , shell=True, stdout=DEVNULL)
    if not noerror == 0:
        return False
    noerror = call('sudo firewall-cmd --permanent --add-rich-rule="rule family=\'{}\' source address=\'{}\' reject"'.format(iptype, ip), shell=True, stdout=DEVNULL)
    if noerror == 0:
        return True
    return False
    

# use the ufw to block the IP
# ip:       IP to block
def useUfw(ip: str) -> bool:
    if not ip:
        return False
    # check if every comand is realy called successfully
    noerror = call('sudo ufw reject from {} to any'.format(ip), shell=True, stdout=DEVNULL)
    if not noerror == 0:
        return False
    noerror = call('sudo ufw reject out to {}'.format(ip), shell=True, stdout=DEVNULL)
    if noerror == 0:
        return True
    return False


# check if firewalld or ufw is used
def checkFirewall() -> str:
    firewall = 'none'
    firewallstatus = call('systemctl status firewalld', shell=True, stdout=DEVNULL, stderr=DEVNULL)
    if firewallstatus == 0:
        print('Using firewalld')
        firewall = 'firewalld'
    else:
        firewallstatus = call('systemctl status ufw', shell=True, stdout=DEVNULL, stderr=DEVNULL)
        if firewallstatus == 0:
            print('Using ufw')
            firewall = 'ufw'
        else:
            print('It seems your firewall is inactive! Please make shure you are using ufw or firewalld as your firewall!')
    return firewall


# check if file exists and read the file
# filename:     path to the file
def readFile(filename: str) -> bool or list:
    if not filename:
        print('Please specify a file!')
        return False
    
    try:
        file = open(filename, 'r')
        lines = len(open(filename).readlines())
    except IOError:
        print('Cannot open or find file! ', filename)
        return False
    
    if not file.readable() or not filename.endswith('.txt'):
        print('Your file is not in a readable format, please use .txt as your filetype!')
        return False
    
    args = []
    for i in range(0, lines):
        args.append(str(file.readline()).replace('\n', ''))

    if len(args) == 0:
        print('Your file seems to be empty!')
        return False
    return args


# extract the ip addresses from the subnet (if its exists) and validate all ip addresses
# args:     The indicated user input which should contains ip addresses
# @return:  Returns a list of all valid ip addresses
def extractAndValidateIPs(args) -> list:
    """
    Extract the ip addresses from the subnet (if its exists) and validate all ip addresses
    """
    ips = []
    invalidIps = []
    for singleinput in args:
        try:
            subips = IPNetwork(singleinput)
        except AddrFormatError as e:
            # Assume that the entry is a domain
            subips = getIpFromDNS(singleinput)

            if isinstance(subips, int):
                invalidIps.append(singleinput)
                continue
        
        if isinstance(subips, str):
            ips.append(subips)
        else:
            for ip in subips:
                ips.append(str(ip))
    if len(invalidIps) > 0:
        print('The following ip address(es) are invalid: ', invalidIps)
    return ips


def getIpFromDNS(domainName: str) -> int or str:
    """
    Try to get the ip/s from the given domain name
    If it is not a domain, returns errorcode
    """
    if not isinstance(domainName, str):
        return -1
    try:
        return socket.gethostbyname(domainName)
    except socket.gaierror as e:
        return -2


def help() -> None:
    """
    Prints the helptext if the '--help' or '-h' condition is set
    """

    helptext = """
    Simple python script for blocking IP addresses with your firewall (ufw or firewalld)
    Please specify a IP address or a domain which you want to be blocked\n\n
    \033[1mSyntax:\033[0m\n
    -h  --help:              Displaying this help page
    -v  --version:           Show the version of this script
    -f  --file:              Read a .txt file including the IPs to block\n
    \033[1mUsing:\033[0m\n
    Execute the script with python version 3.7 or later!
    In order to block a IP or an IP area please specify one or more IPs like: 
    python3 path/to/the/sctipt 192.178.168.0 192.168.178.1 [...]
    or 
    python3 path/to/the/sctipt 192.178.168.0/24 to block the whole network area (this requires special knowledge!)
    You can also block a IP from a domain like:
    python3 path/to/the/sctipt google.com doubleclick.net [...]\n
    \033[1mRequirements:\033[0m\n
    This script requires this python libaries:
    socket, netaddr, subprocess and sys\n
    You also need either firewalld or ufw as your firewall enabled!\n
    At the time this script requires systemctl (systemd) but support for runit will come in the future!\n\n
    Have fun with this script but be carefull!
    """

    print(helptext)


# init-function, the startpoint of the script
# argv:     indicated start parameter
def __init__(argv: list) -> None:
    if not argv or len(argv) < 2:
        print('Please specify at least one IP to block')
        quit()
    # parsing user input in string array to escape the input
    args = []
    del argv[0]
    for arg in argv:
        args.append(str(arg))
    if args[0] == '-h' or args[0] == '--help':
        help()

    elif args[0] == '-v' or args[0] == '--version':
        print('Version 1.7.0')

    elif args[0] == '-f' or args[0] == '--file':
        if len(args) <= 1:
            print('Please specify a file!')
            quit()

        ips = readFile(args[1])
        ips = extractAndValidateIPs(ips)
        if ips:
            blockTheIP(ips)
        else:
            quit()
    else:
        ips = extractAndValidateIPs(args)
        blockTheIP(ips)

# start point of the script
# sys.argv: start parameter
__init__(argv)